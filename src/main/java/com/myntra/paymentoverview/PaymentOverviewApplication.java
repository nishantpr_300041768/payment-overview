package com.myntra.paymentoverview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentOverviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentOverviewApplication.class, args);
	}

}
