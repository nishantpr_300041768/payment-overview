package com.myntra.paymentoverview.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import lombok.Data;


import java.util.List;

@Data
@XmlRootElement(name="paymentDetailResponse")
public class PaymentDetailsResponse extends AbstractResponse {
//    @XmlElementWrapper(name = "data")
//    @XmlElement(name = "settlementAmountEntry")
//    List<SettlementAmountEntry> data;
}
