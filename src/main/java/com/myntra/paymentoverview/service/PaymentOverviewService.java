package com.myntra.paymentoverview.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.myntra.commons.response.AbstractResponse;

@Path("/payment-overview/v1")
public interface PaymentOverviewService {
    @GET
    @Produces({"application/xml", "application/json"})
    @Path("/payment-history/seller/{sellerId}")
    AbstractResponse getListOfPayments(@PathParam("sellerId") Long sellerId);

    @GET
    @Produces({"application/xml", "application/json"})
    @Path("/outstanding-payment/seller/{sellerId}")
    AbstractResponse getOutstandingPayment(@PathParam("sellerId") Long sellerId);

    @GET
    @Produces({"application/xml", "application/json"})
    @Path("/next-payment/seller/{sellerId}")
    AbstractResponse getNextPayment(@PathParam("sellerId") Long sellerId);

    @GET
    @Produces({"application/xml", "application/json"})
    @Path("/payment-breakup/utr/{utrNum}")
    AbstractResponse getPaymentBreakup(@PathParam("utrNum") Long sellerId);

}
