package com.myntra.paymentoverview.service.impl;

import com.myntra.paymentoverview.entity.PaymentDetailsResponse;
import com.myntra.paymentoverview.service.PaymentOverviewService;

public class PaymentOverviewServiceImpl implements PaymentOverviewService {

    @Override
    public PaymentDetailsResponse getListOfPayments(Long sellerId) {
        return null;
    }

    @Override
    public PaymentDetailsResponse getOutstandingPayment(Long sellerId) {
        return null;
    }

    @Override
    public PaymentDetailsResponse getNextPayment(Long sellerId) {
        return null;
    }

    @Override
    public PaymentDetailsResponse getPaymentBreakup(Long sellerId) {
        return null;
    }
}
